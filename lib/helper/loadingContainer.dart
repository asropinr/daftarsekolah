import 'package:flutter/material.dart';

class LoadingContainer extends StatelessWidget {
  final bool isLoading;
  const LoadingContainer({@required this.isLoading, Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(color: Colors.white),
      width: isLoading ? MediaQuery.of(context).size.width : 0,
      height: isLoading ? MediaQuery.of(context).size.height : 0,
      child: Center(
        child: CircularProgressIndicator(),
      ),
    );
  }
}
