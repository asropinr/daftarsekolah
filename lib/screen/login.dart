import 'package:daftarsekolah/controller/siswaController.dart';
import 'package:daftarsekolah/helper/size.dart';
import 'package:daftarsekolah/screen/admin/adminPage.dart';
import 'package:daftarsekolah/screen/admin/dataSiswa/form.dart';
import 'package:daftarsekolah/screen/siswa/siswaPage.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class LoginPage extends StatefulWidget {
  LoginPage({Key key}) : super(key: key);

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  TextEditingController nisn = TextEditingController();

  SiswaController siswaController = Get.put(SiswaController());
  bool enable = true;

  login() async {
    await siswaController.auth(nisn.text);

    if (siswaController.logg == 1) {
      Navigator.push(
          context, MaterialPageRoute(builder: (context) => SiswaPage()));
    }
  }

  validation() {
    if (nisn.text == "admin") {
      Navigator.push(
          context, MaterialPageRoute(builder: (context) => AdminPage()));
    } else {
      login();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFF51F1FF),
      body: Center(
        child: Container(
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(AdaptiveSize.width(10)),
            boxShadow: [
              BoxShadow(
                color: Colors.grey.withOpacity(0.5),
                spreadRadius: 1,
                blurRadius: 1,
                offset: Offset(1, 1),
              ),
            ],
          ),
          padding: EdgeInsets.all(AdaptiveSize.width(16)),
          height: AdaptiveSize.height(350),
          width: AdaptiveSize.width(300),
          child: Column(
            children: [
              Text(
                "LOGIN",
                style: TextStyle(fontSize: 30, fontWeight: FontWeight.bold),
              ),
              SizedBox(
                height: AdaptiveSize.height(20),
              ),
              TextField(
                controller: nisn,
                onSubmitted: (value) {
                  nisn.text = value;
                },
                decoration: InputDecoration(
                    labelText: "NISN",
                    labelStyle: TextStyle(
                      fontSize: 14,
                      color: Colors.black,
                    ),
                    floatingLabelBehavior: FloatingLabelBehavior.always),
              ),
              // SizedBox(
              //   height: AdaptiveSize.height(20),
              // ),
              // TextField(
              //   controller: password,
              //   obscureText: true,
              //   obscuringCharacter: "*",
              //   decoration: InputDecoration(
              //       labelText: "Password",
              //       labelStyle: TextStyle(
              //         fontSize: 14,
              //         color: Colors.black,
              //       ),
              //       floatingLabelBehavior: FloatingLabelBehavior.always),
              // ),
              SizedBox(
                height: AdaptiveSize.height(30),
              ),
              GestureDetector(
                onTap: () => validation(),
                child: Container(
                  width: AdaptiveSize.width(50),
                  height: AdaptiveSize.height(30),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(AdaptiveSize.width(5)),
                    color: Colors.blueAccent,
                  ),
                  child: Center(
                    child: Text(
                      "Login",
                      style: TextStyle(
                        fontSize: 16,
                        color: Colors.white,
                      ),
                    ),
                  ),
                ),
              ),
              SizedBox(
                height: AdaptiveSize.height(10),
              ),
              GestureDetector(
                onTap: () => Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => FormDataSiswa(
                      isEdit: false,
                      isRegis: true,
                    ),
                  ),
                ),
                child: Container(
                  width: AdaptiveSize.width(60),
                  height: AdaptiveSize.height(30),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(AdaptiveSize.width(5)),
                    color: Colors.greenAccent,
                  ),
                  child: Center(
                    child: Text(
                      "Register",
                      style: TextStyle(
                        fontSize: 16,
                        color: Colors.white,
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
