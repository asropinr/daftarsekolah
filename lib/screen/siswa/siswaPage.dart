import 'package:daftarsekolah/controller/siswaController.dart';
import 'package:daftarsekolah/helper/size.dart';
import 'package:daftarsekolah/screen/siswa/daftarPenjurusan.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class SiswaPage extends StatefulWidget {
  SiswaPage({
    Key key,
  }) : super(key: key);

  @override
  _SiswaPageState createState() => _SiswaPageState();
}

class _SiswaPageState extends State<SiswaPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text("SISWA PAGE"),
      ),
      body: Container(
        margin: EdgeInsets.symmetric(
          horizontal: AdaptiveSize.width(16),
          vertical: AdaptiveSize.height(20),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            GestureDetector(
              onTap: () => Navigator.push(context,
                  MaterialPageRoute(builder: (context) => DaftarPenjurusan())),
              child: Container(
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(
                    AdaptiveSize.width(10),
                  ),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey.withOpacity(0.5),
                      spreadRadius: 1,
                      blurRadius: 1,
                      offset: Offset(1, 1),
                    ),
                  ],
                ),
                height: AdaptiveSize.height(100),
                width: AdaptiveSize.width(200),
                child: Center(
                  child: Text(
                    "DAFTAR PENJURUSAN",
                    style: TextStyle(
                      fontSize: 20,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
