import 'package:daftarsekolah/controller/siswaController.dart';
import 'package:daftarsekolah/helper/loadingContainer.dart';
import 'package:daftarsekolah/helper/size.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

class DaftarPenjurusan extends StatefulWidget {
  DaftarPenjurusan({Key key}) : super(key: key);

  @override
  _DaftarPenjurusanState createState() => _DaftarPenjurusanState();
}

class _DaftarPenjurusanState extends State<DaftarPenjurusan> {
  SiswaController siswaController = Get.find();
  bool isLoading = true;

  getdata() async {
    await siswaController.getDataJurusan();

    setState(() {
      isLoading = false;
    });
  }

  @override
  void initState() {
    super.initState();
    getdata();
  }

  List penjurusan = [
    {
      "id": 1,
      "name": "MIA",
      "Maxkuota": 50,
      "Jumlah_siswa": 30,
    },
    {
      "id": 2,
      "name": "IIS",
      "Maxkuota": 50,
      "Jumlah_siswa": 50,
    },
    {
      "id": 3,
      "name": "IB",
      "Maxkuota": 50,
      "Jumlah_siswa": 30,
    }
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text("LIST PENJURUSAN"),
      ),
      body: Builder(
        builder: (_) {
          if (isLoading) {
            return LoadingContainer(isLoading: isLoading);
          } else {
            return ListView(
              children: [
                SizedBox(
                  height: AdaptiveSize.height(20),
                ),
                Column(
                  children: List.generate(
                    siswaController.jur.length,
                    (index) => CardDataPenjurusan(
                      data: siswaController.jur,
                      index: index,
                    ),
                  ),
                ),
                // SizedBox(
                //   height: AdaptiveSize.height(20),
                // ),
                // GestureDetector(
                //   onTap: () => null,
                //   //enable ? showSearchDialogSukses() : showSearchDialogPenuh(),
                //   child: Container(
                //     margin: EdgeInsets.symmetric(
                //         horizontal: AdaptiveSize.width(16)),
                //     width: MediaQuery.of(context).size.width,
                //     height: AdaptiveSize.height(50),
                //     decoration: BoxDecoration(
                //       borderRadius:
                //           BorderRadius.circular(AdaptiveSize.width(5)),
                //       color: Colors.blueAccent,
                //       //color: enable ? Colors.greenAccent : Colors.grey,
                //     ),
                //     child: Center(
                //       child: Text(
                //         "Tambah",
                //         style: TextStyle(
                //           fontSize: 16,
                //           color: Colors.white,
                //         ),
                //       ),
                //     ),
                //   ),
                // )
              ],
            );
          }
        },
      ),
    );
  }
}

class CardDataPenjurusan extends StatefulWidget {
  final data;
  final index;
  CardDataPenjurusan({
    Key key,
    @required this.data,
    @required this.index,
  }) : super(key: key);

  @override
  _CardDataPenjurusanState createState() => _CardDataPenjurusanState();
}

class _CardDataPenjurusanState extends State<CardDataPenjurusan> {
  // bool enable;

  // validation() {
  //   if (widget.data[widget.index]["Maxkuota"] >
  //       widget.data[widget.index]["Jumlah_siswa"]) {
  //     setState(() {
  //       enable = true;
  //     });
  //   } else {
  //     enable = false;
  //   }
  // }

  SiswaController siswaController = Get.find();
  int selected;

  createData() async {
    await siswaController.createPenjurusan(
        siswaController.login[0]["id_siswa"], widget.data[selected]["id"]);
  }

  showSearchDialogPenuh() async {
    return showDialog<void>(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return Dialog(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(AdaptiveSize.width(10)),
            ),
            child: Container(
              padding: EdgeInsets.all(
                AdaptiveSize.width(16),
              ),
              height: AdaptiveSize.width(120),
              child: Column(
                children: [
                  SizedBox(
                    height: AdaptiveSize.height(10),
                  ),
                  Text("Maaf Kelas Sudah Penuh"),
                  SizedBox(
                    height: AdaptiveSize.height(20),
                  ),
                  GestureDetector(
                    onTap: () => Navigator.of(context).pop(),
                    child: Container(
                      width: AdaptiveSize.width(100),
                      height: AdaptiveSize.height(30),
                      decoration: BoxDecoration(
                        borderRadius:
                            BorderRadius.circular(AdaptiveSize.width(5)),
                        color: Colors.blueAccent,
                      ),
                      child: Center(
                        child: Text(
                          "OK",
                          style: TextStyle(
                            fontSize: 20,
                            color: Colors.white,
                          ),
                        ),
                      ),
                    ),
                  )
                ],
              ),
            ),
          );
        });
  }

  showSearchDialogSukses() async {
    return showDialog<void>(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return Dialog(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(AdaptiveSize.width(10)),
            ),
            child: Container(
              padding: EdgeInsets.all(
                AdaptiveSize.width(16),
              ),
              height: AdaptiveSize.width(120),
              child: Column(
                children: [
                  SizedBox(
                    height: AdaptiveSize.height(10),
                  ),
                  Text("Anda Sudah Terdaftar"),
                  SizedBox(
                    height: AdaptiveSize.height(20),
                  ),
                  GestureDetector(
                    onTap: () {
                      createData();
                      Navigator.of(context).pop();
                      Navigator.of(context).pop();
                    },
                    child: Container(
                      width: AdaptiveSize.width(100),
                      height: AdaptiveSize.height(30),
                      decoration: BoxDecoration(
                        borderRadius:
                            BorderRadius.circular(AdaptiveSize.width(5)),
                        color: Colors.blueAccent,
                      ),
                      child: Center(
                        child: Text(
                          "OK",
                          style: TextStyle(
                            fontSize: 20,
                            color: Colors.white,
                          ),
                        ),
                      ),
                    ),
                  )
                ],
              ),
            ),
          );
        });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    //validation();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(
        AdaptiveSize.width(10),
      ),
      margin: EdgeInsets.all(
        AdaptiveSize.width(10),
      ),
      decoration: BoxDecoration(
          color: Colors.white,
          boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(0.5),
              spreadRadius: 1,
              blurRadius: 1,
              offset: Offset(1, 1),
            ),
          ],
          borderRadius: BorderRadius.circular(AdaptiveSize.width(10))),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                "Nama : ${widget.data[widget.index]["nama"]}",
                style: TextStyle(
                  fontSize: 14,
                  fontWeight: FontWeight.bold,
                ),
              ),
              SizedBox(
                height: AdaptiveSize.height(5),
              ),
              Text(
                "Max Kuota : ${widget.data[widget.index]["kuota"]}",
                style: TextStyle(
                  fontSize: 14,
                ),
              ),
              SizedBox(
                height: AdaptiveSize.height(5),
              ),
              Text(
                "Jumlah Siswa : ${widget.data[widget.index]["jumlah_sekarang"]}",
                style: TextStyle(
                  fontSize: 14,
                ),
              ),
            ],
          ),
          Builder(builder: (_) {
            if (int.parse(widget.data[widget.index]["kuota"]) >
                int.parse(widget.data[widget.index]["jumlah_sekarang"])) {
              return GestureDetector(
                onTap: () {
                  setState(() {
                    selected = widget.index;
                  });
                  showSearchDialogSukses();
                },
                //enable ? showSearchDialogSukses() : showSearchDialogPenuh(),
                child: Container(
                  width: AdaptiveSize.width(60),
                  height: AdaptiveSize.height(60),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(AdaptiveSize.width(5)),
                    color: Colors.greenAccent,
                    //color: enable ? Colors.greenAccent : Colors.grey,
                  ),
                  child: Center(
                    child: Text(
                      "Daftar",
                      style: TextStyle(
                        fontSize: 16,
                        color: Colors.white,
                      ),
                    ),
                  ),
                ),
              );
            } else {
              return GestureDetector(
                onTap: () => showSearchDialogPenuh(),
                //enable ? showSearchDialogSukses() : showSearchDialogPenuh(),
                child: Container(
                  width: AdaptiveSize.width(60),
                  height: AdaptiveSize.height(60),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(AdaptiveSize.width(5)),
                    color: Colors.grey,
                    //color: enable ? Colors.greenAccent : Colors.grey,
                  ),
                  child: Center(
                    child: Text(
                      "Daftar",
                      style: TextStyle(
                        fontSize: 16,
                        color: Colors.white,
                      ),
                    ),
                  ),
                ),
              );
            }
          })
        ],
      ),
    );
  }
}
