import 'package:daftarsekolah/controller/siswaController.dart';
import 'package:daftarsekolah/helper/size.dart';
import 'package:daftarsekolah/screen/admin/dataPenjurusan/listPenjurusan.dart';
import 'package:daftarsekolah/screen/admin/dataSiswa/listSiswa.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class AdminPage extends StatefulWidget {
  AdminPage({Key key}) : super(key: key);

  @override
  _AdminPageState createState() => _AdminPageState();
}

class _AdminPageState extends State<AdminPage> {
  SiswaController siswaController = Get.put(SiswaController());

  getdata() async {
    await siswaController.getListSiswa();
  }

  deleteData() async {}

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getdata();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text("ADMIN PAGE"),
      ),
      body: Container(
        margin: EdgeInsets.symmetric(
          horizontal: AdaptiveSize.width(16),
          vertical: AdaptiveSize.height(20),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            GestureDetector(
              onTap: () => Navigator.push(context,
                  MaterialPageRoute(builder: (context) => ListSiswa())),
              child: Container(
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(
                    AdaptiveSize.width(10),
                  ),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey.withOpacity(0.5),
                      spreadRadius: 1,
                      blurRadius: 1,
                      offset: Offset(1, 1),
                    ),
                  ],
                ),
                height: AdaptiveSize.height(200),
                width: AdaptiveSize.width(150),
                child: Center(
                  child: Text(
                    "DATA SISWA",
                    style: TextStyle(
                      fontSize: 20,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
              ),
            ),
            GestureDetector(
              onTap: () => Navigator.push(context,
                  MaterialPageRoute(builder: (context) => ListPenjurusan())),
              child: Container(
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(
                    AdaptiveSize.width(10),
                  ),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey.withOpacity(0.5),
                      spreadRadius: 1,
                      blurRadius: 1,
                      offset: Offset(1, 1),
                    ),
                  ],
                ),
                height: AdaptiveSize.height(200),
                width: AdaptiveSize.width(150),
                child: Center(
                  child: Text(
                    "PENJURUSAN",
                    style: TextStyle(
                      fontSize: 20,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
