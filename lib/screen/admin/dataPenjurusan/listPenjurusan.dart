import 'package:daftarsekolah/controller/jurusanController.dart';
import 'package:daftarsekolah/helper/loadingContainer.dart';
import 'package:daftarsekolah/helper/size.dart';
import 'package:daftarsekolah/screen/admin/dataPenjurusan/formPenjurusan.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ListPenjurusan extends StatefulWidget {
  ListPenjurusan({Key key}) : super(key: key);

  @override
  _ListPenjurusanState createState() => _ListPenjurusanState();
}

class _ListPenjurusanState extends State<ListPenjurusan> {
  JurusanController jurusanController = Get.put(JurusanController());
  bool isLoading = true;

  getData() async {
    await jurusanController.getDataJurusan();

    setState(() {
      isLoading = false;
    });
  }

  @override
  void initState() {
    super.initState();
    getData();
  }

  List penjurusan = [
    {
      "id": 1,
      "name": "MIA",
      "Maxkuota": 50,
      "Jumlah_siswa": 30,
    },
    {
      "id": 2,
      "name": "IIS",
      "Maxkuota": 50,
      "Jumlah_siswa": 50,
    },
    {
      "id": 3,
      "name": "IB",
      "Maxkuota": 50,
      "Jumlah_siswa": 30,
    }
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: Text("LIST PENJURUSAN"),
        ),
        body: Builder(builder: (_) {
          if (isLoading) {
            return LoadingContainer(isLoading: isLoading);
          } else {
            return ListView(
              children: [
                SizedBox(
                  height: AdaptiveSize.height(20),
                ),
                GestureDetector(
                  onTap: () => Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => FormDataPenjurusan(),
                    ),
                  ),
                  child: Container(
                    margin: EdgeInsets.only(left: AdaptiveSize.width(150)),
                    padding: EdgeInsets.symmetric(
                      horizontal: AdaptiveSize.width(16),
                    ),
                    child: Row(
                      children: [
                        Icon(
                          Icons.add,
                          color: Colors.black,
                        ),
                        Text(
                          "Tambah Penjurusan",
                          style: TextStyle(
                            fontSize: 20,
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                SizedBox(
                  height: AdaptiveSize.height(10),
                ),
                Column(
                  children: List.generate(
                    jurusanController.jurusan.length,
                    (index) => CardDataPenjurusan(
                      data: jurusanController.jurusan,
                      index: index,
                    ),
                  ),
                ),
              ],
            );
          }
        }));
  }
}

class CardDataPenjurusan extends StatefulWidget {
  final data;
  final index;
  CardDataPenjurusan({
    Key key,
    @required this.data,
    @required this.index,
  }) : super(key: key);

  @override
  _CardDataPenjurusanState createState() => _CardDataPenjurusanState();
}

class _CardDataPenjurusanState extends State<CardDataPenjurusan> {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(
        AdaptiveSize.width(10),
      ),
      margin: EdgeInsets.all(
        AdaptiveSize.width(10),
      ),
      decoration: BoxDecoration(
          color: Colors.white,
          boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(0.5),
              spreadRadius: 1,
              blurRadius: 1,
              offset: Offset(1, 1),
            ),
          ],
          borderRadius: BorderRadius.circular(AdaptiveSize.width(10))),
      child: Row(
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                "Nama : ${widget.data[widget.index]["nama_jur"]}",
                style: TextStyle(
                  fontSize: 14,
                  fontWeight: FontWeight.bold,
                ),
              ),
              SizedBox(
                height: AdaptiveSize.height(5),
              ),
              Text(
                "Max Kuota : ${widget.data[widget.index]["max_kuota"]}",
                style: TextStyle(
                  fontSize: 14,
                ),
              ),
              SizedBox(
                height: AdaptiveSize.height(5),
              ),
            ],
          )
        ],
      ),
    );
  }
}
