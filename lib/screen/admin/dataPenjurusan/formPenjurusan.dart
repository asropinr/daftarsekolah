import 'package:daftarsekolah/controller/jurusanController.dart';
import 'package:daftarsekolah/helper/size.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class FormDataPenjurusan extends StatefulWidget {
  // final Map siswa;
  // final bool isEdit;
  FormDataPenjurusan({
    Key key,
    // @required this.isEdit,
    // this.siswa,
  }) : super(key: key);

  @override
  FormDataPenjurusanState createState() => FormDataPenjurusanState();
}

class FormDataPenjurusanState extends State<FormDataPenjurusan> {
  JurusanController jurusanController = Get.find();
  TextEditingController nama = TextEditingController();
  TextEditingController maxkuota = TextEditingController();

  createJurusan() async {
    await jurusanController.createJurusan(nama.text, int.parse(maxkuota.text));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text("FORM DATA PENJURUSAN"),
      ),
      body: ListView(
        children: [
          Container(
            padding: EdgeInsets.all(
              AdaptiveSize.width(16),
            ),
            child: Column(
              children: [
                TextField(
                  controller: nama,
                  decoration: InputDecoration(
                      labelText: "Nama",
                      hintText: "Nama Tidak boleh lebih dari 5 huruf",
                      labelStyle: TextStyle(
                        fontSize: 14,
                        color: Colors.black,
                      ),
                      floatingLabelBehavior: FloatingLabelBehavior.always),
                ),
                SizedBox(
                  height: AdaptiveSize.height(10),
                ),
                TextField(
                  keyboardType: TextInputType.number,
                  controller: maxkuota,
                  decoration: InputDecoration(
                      labelText: "Max Kuota",
                      labelStyle: TextStyle(
                        fontSize: 14,
                        color: Colors.black,
                      ),
                      floatingLabelBehavior: FloatingLabelBehavior.always),
                ),
                SizedBox(
                  height: AdaptiveSize.height(30),
                ),
                GestureDetector(
                  onTap: () {
                    createJurusan();
                    Navigator.of(context).pop();
                    Navigator.of(context).pop();
                  },
                  child: Container(
                    width: MediaQuery.of(context).size.width,
                    height: AdaptiveSize.height(50),
                    decoration: BoxDecoration(
                      borderRadius:
                          BorderRadius.circular(AdaptiveSize.width(5)),
                      color: Colors.blueAccent,
                    ),
                    child: Center(
                      child: Text(
                        "Simpan",
                        style: TextStyle(
                          fontSize: 16,
                          color: Colors.white,
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
