import 'package:daftarsekolah/controller/siswaController.dart';
import 'package:daftarsekolah/helper/loadingContainer.dart';
import 'package:daftarsekolah/helper/size.dart';
import 'package:daftarsekolah/screen/admin/dataSiswa/form.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ListSiswa extends StatefulWidget {
  ListSiswa({Key key}) : super(key: key);

  @override
  _ListSiswaState createState() => _ListSiswaState();
}

class _ListSiswaState extends State<ListSiswa> {
  SiswaController siswaController = Get.find();
  bool isLoading = true;

  getdata() async {
    setState(() {
      isLoading = true;
    });

    await siswaController.getListSiswa();

    setState(() {
      isLoading = false;
    });
  }

  List siswa = [
    {
      "id": 1,
      "nama": "Agung",
      "alamat": "JL tluki 1",
      "notelp": 085555666777,
      "sekolah_asal": "SMAN 1 Depok",
      "nama_ortu": "Bambang",
    },
    {
      "id": 2,
      "nama": "Ayam",
      "alamat": "JL tluki 2",
      "notelp": 085555666779,
      "sekolah_asal": "SMAN 2 Depok",
      "nama_ortu": "Arif",
    },
    {
      "id": 3,
      "nama": "Dayat",
      "alamat": "JL tluki 4",
      "notelp": 085555666777,
      "sekolah_asal": "SMAN 5 Depok",
      "nama_ortu": "Setyo",
    },
    {
      "id": 4,
      "nama": "Dayat",
      "alamat": "JL tluki 4",
      "notelp": 085555666780,
      "sekolah_asal": "SMAN 5 Depok",
      "nama_ortu": "Setyo",
    },
    {
      "id": 5,
      "nama": "Dayat",
      "alamat": "JL tluki 4",
      "notelp": 085555666781,
      "sekolah_asal": "SMAN 5 Depok",
      "nama_ortu": "Setyo",
    }
  ];

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getdata();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text("LIST DATA SISWA"),
      ),
      body: Builder(builder: (_) {
        if (isLoading) {
          return LoadingContainer(isLoading: isLoading);
        } else {
          return ListView(
            children: [
              SizedBox(
                height: AdaptiveSize.height(10),
              ),
              GestureDetector(
                onTap: () => Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => FormDataSiswa(
                      isEdit: false,
                      isRegis: false,
                    ),
                  ),
                ),
                child: Container(
                  margin: EdgeInsets.only(left: AdaptiveSize.width(200)),
                  padding: EdgeInsets.symmetric(
                    horizontal: AdaptiveSize.width(16),
                  ),
                  child: Row(
                    children: [
                      Icon(
                        Icons.add,
                        color: Colors.black,
                      ),
                      Text(
                        "Tambah Siswa",
                        style: TextStyle(
                          fontSize: 20,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              Column(
                children: List.generate(
                    siswaController.siswa.length,
                    (index) => GestureDetector(
                          onTap: () async {
                            await Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) => FormDataSiswa(
                                  isEdit: true,
                                  isRegis: false,
                                  siswa: siswaController.siswa[index],
                                ),
                              ),
                            );
                            getdata();
                          },
                          child: CardSiswa(
                            siswa: siswaController.siswa,
                            index: index,
                          ),
                        )),
              ),
            ],
          );
        }
      }),
    );
  }
}

class CardSiswa extends StatefulWidget {
  final siswa;
  final index;
  CardSiswa({
    Key key,
    @required this.siswa,
    @required this.index,
  }) : super(key: key);

  @override
  _CardSiswaState createState() => _CardSiswaState();
}

class _CardSiswaState extends State<CardSiswa> {
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          color: Colors.white,
          boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(0.5),
              spreadRadius: 1,
              blurRadius: 1,
              offset: Offset(1, 1),
            ),
          ],
          borderRadius: BorderRadius.circular(AdaptiveSize.width(10))),
      margin: EdgeInsets.all(
        AdaptiveSize.width(16),
      ),
      child: Column(
        children: [
          Row(
            children: [
              Container(
                margin: EdgeInsets.all(AdaptiveSize.width(10)),
                decoration: BoxDecoration(
                    color: Colors.grey,
                    borderRadius:
                        BorderRadius.circular(AdaptiveSize.width(10))),
                height: AdaptiveSize.height(120),
                width: AdaptiveSize.width(120),
              ),
              Container(
                child: Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "NISN : ${widget.siswa[widget.index]["nisn"]}",
                        style: TextStyle(
                          fontSize: 14,
                        ),
                      ),
                      SizedBox(
                        height: AdaptiveSize.height(5),
                      ),
                      Text(
                        "Nama : ${widget.siswa[widget.index]["nama_siswa"]}",
                        style: TextStyle(
                          fontSize: 14,
                        ),
                      ),
                      SizedBox(
                        height: AdaptiveSize.height(5),
                      ),
                      Text(
                        "Alamat : ${widget.siswa[widget.index]["alamat_siswa"]}",
                        style: TextStyle(
                          fontSize: 14,
                        ),
                      ),
                      SizedBox(
                        height: AdaptiveSize.height(5),
                      ),
                      Text(
                        "Sekolah Asal : ${widget.siswa[widget.index]["sekolah_asal"]}",
                        style: TextStyle(
                          fontSize: 14,
                        ),
                      ),
                      SizedBox(
                        height: AdaptiveSize.height(5),
                      ),
                      Text(
                        "Nama Orang tua : ${widget.siswa[widget.index]["nama_ortu"]}",
                        style: TextStyle(
                          fontSize: 14,
                        ),
                      ),
                      SizedBox(
                        height: AdaptiveSize.height(5),
                      ),
                      Text(
                        "No Telpon : ${widget.siswa[widget.index]["no_telp"]}",
                        style: TextStyle(
                          fontSize: 14,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
          // Container(
          //   padding: EdgeInsets.symmetric(
          //     horizontal: AdaptiveSize.width(15),
          //   ),
          //   child: Row(
          //     mainAxisAlignment: MainAxisAlignment.spaceBetween,
          //     children: [
          //       Text(
          //         "Action",
          //         style: TextStyle(
          //           fontSize: 18,
          //           fontWeight: FontWeight.bold,
          //         ),
          //       ),
          //       GestureDetector(
          //         onTap: () =>
          //         ),
          //         child: Container(
          //           width: AdaptiveSize.width(50),
          //           height: AdaptiveSize.height(30),
          //           decoration: BoxDecoration(
          //             borderRadius:
          //                 BorderRadius.circular(AdaptiveSize.width(5)),
          //             color: Colors.greenAccent,
          //           ),
          //           child: Center(
          //             child: Text(
          //               "Edit",
          //               style: TextStyle(
          //                 fontSize: 16,
          //                 color: Colors.white,
          //               ),
          //             ),
          //           ),
          //         ),
          //       ),
          //       //old action
          //       // Row(
          //       //   children: [
          //       //     GestureDetector(
          //       //       onTap: () => Navigator.push(
          //       //         context,
          //       //         MaterialPageRoute(
          //       //           builder: (context) => FormDataSiswa(
          //       //             isEdit: true,
          //       //             siswa: widget.siswa[widget.index],
          //       //           ),
          //       //         ),
          //       //       ),
          //       //       child: Container(
          //       //         width: AdaptiveSize.width(50),
          //       //         height: AdaptiveSize.height(30),
          //       //         decoration: BoxDecoration(
          //       //           borderRadius:
          //       //               BorderRadius.circular(AdaptiveSize.width(5)),
          //       //           color: Colors.greenAccent,
          //       //         ),
          //       //         child: Center(
          //       //           child: Text(
          //       //             "Edit",
          //       //             style: TextStyle(
          //       //               fontSize: 16,
          //       //               color: Colors.white,
          //       //             ),
          //       //           ),
          //       //         ),
          //       //       ),
          //       //     ),
          //       //     SizedBox(
          //       //       width: AdaptiveSize.width(10),
          //       //     ),
          //       //     GestureDetector(
          //       //       onTap: () => null,
          //       //       child: Container(
          //       //         width: AdaptiveSize.width(50),
          //       //         height: AdaptiveSize.height(30),
          //       //         decoration: BoxDecoration(
          //       //           borderRadius:
          //       //               BorderRadius.circular(AdaptiveSize.width(5)),
          //       //           color: Colors.redAccent,
          //       //         ),
          //       //         child: Center(
          //       //           child: Text(
          //       //             "Hapus",
          //       //             style: TextStyle(
          //       //               fontSize: 16,
          //       //               color: Colors.white,
          //       //             ),
          //       //           ),
          //       //         ),
          //       //       ),
          //       //     ),
          //       //   ],
          //       // )
          //     ],
          //   ),
          // ),
          // SizedBox(
          //   height: AdaptiveSize.height(10),
          // ),
        ],
      ),
    );
  }
}
