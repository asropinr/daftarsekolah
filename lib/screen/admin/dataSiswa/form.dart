import 'dart:io';
import 'package:daftarsekolah/controller/siswaController.dart';
import 'package:daftarsekolah/helper/size.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';

class FormDataSiswa extends StatefulWidget {
  final Map siswa;
  final bool isEdit;
  final bool isRegis;

  FormDataSiswa({
    Key key,
    @required this.isEdit,
    @required this.isRegis,
    this.siswa,
  }) : super(key: key);

  @override
  _FormDataSiswaState createState() => _FormDataSiswaState();
}

class _FormDataSiswaState extends State<FormDataSiswa> {
  TextEditingController nama = TextEditingController();
  TextEditingController alamat = TextEditingController();
  TextEditingController namaortu = TextEditingController();
  TextEditingController asalsekolah = TextEditingController();
  TextEditingController notelp = TextEditingController();
  TextEditingController nisn = TextEditingController();

  SiswaController siswaController = Get.find();

  bool isEdit;
  bool isRegis;
  final ImagePicker imagePicker = ImagePicker();
  File _image;

  deleteData() async {
    await siswaController.deleteDataSiswa(widget.siswa["id_siswa"]);
  }

  updateData() async {
    await siswaController.putDataSiswa(
        widget.siswa["id_siswa"],
        nama.text,
        "asd",
        alamat.text,
        int.parse(notelp.text),
        asalsekolah.text,
        namaortu.text,
        nisn.text);
  }

  createData() async {
    await siswaController.createDataSiswa(nama.text, "asd", alamat.text,
        int.parse(notelp.text), asalsekolah.text, namaortu.text, nisn.text);
  }

  setData() {
    final Map dataSiswa = widget.siswa;
    nama.text = dataSiswa["nama_siswa"];
    alamat.text = dataSiswa["alamat_siswa"];
    namaortu.text = dataSiswa["nama_ortu"];
    asalsekolah.text = dataSiswa["sekolah_asal"];
    notelp.text = dataSiswa["no_telp"].toString();
    nisn.text = dataSiswa["nisn"];
  }

  setvalue() {
    setState(() {
      isEdit = widget.isEdit;
      isRegis = widget.isRegis;
    });

    if (isEdit) {
      setData();
    }
  }

  _imgFromGallery() async {
    final image = await imagePicker.getImage(
      source: ImageSource.gallery,
      maxHeight: 240,
      maxWidth: 240,
      imageQuality: 100,
    );

    setState(() {
      _image = File(image.path);
    });
  }

  _imgFromCamera() async {
    final image = await imagePicker.getImage(
      source: ImageSource.camera,
      maxHeight: 240,
      maxWidth: 240,
      imageQuality: 100,
    );

    setState(() {
      _image = File(image.path);
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    setvalue();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text("FORM DATA SISWA"),
      ),
      body: ListView(
        children: [
          Container(
            padding: EdgeInsets.all(
              AdaptiveSize.width(16),
            ),
            child: Column(
              children: [
                TextField(
                  controller: nama,
                  decoration: InputDecoration(
                      labelText: "Nama",
                      labelStyle: TextStyle(
                        fontSize: 14,
                        color: Colors.black,
                      ),
                      floatingLabelBehavior: FloatingLabelBehavior.always),
                ),
                SizedBox(
                  height: AdaptiveSize.height(10),
                ),
                TextField(
                  controller: asalsekolah,
                  decoration: InputDecoration(
                      labelText: "Asal Sekolah",
                      labelStyle: TextStyle(
                        fontSize: 14,
                        color: Colors.black,
                      ),
                      floatingLabelBehavior: FloatingLabelBehavior.always),
                ),
                SizedBox(
                  height: AdaptiveSize.height(10),
                ),
                TextField(
                  controller: namaortu,
                  decoration: InputDecoration(
                      labelText: "Nama Orang Tua",
                      labelStyle: TextStyle(
                        fontSize: 14,
                        color: Colors.black,
                      ),
                      floatingLabelBehavior: FloatingLabelBehavior.always),
                ),
                SizedBox(
                  height: AdaptiveSize.height(10),
                ),
                TextField(
                  controller: alamat,
                  decoration: InputDecoration(
                      labelText: "Alamat",
                      labelStyle: TextStyle(
                        fontSize: 14,
                        color: Colors.black,
                      ),
                      floatingLabelBehavior: FloatingLabelBehavior.always),
                ),
                SizedBox(
                  height: AdaptiveSize.height(10),
                ),
                TextField(
                  controller: notelp,
                  keyboardType: TextInputType.number,
                  decoration: InputDecoration(
                      labelText: "No Telepon",
                      labelStyle: TextStyle(
                        fontSize: 14,
                        color: Colors.black,
                      ),
                      floatingLabelBehavior: FloatingLabelBehavior.always),
                ),
                SizedBox(
                  height: AdaptiveSize.height(10),
                ),
                TextField(
                  controller: nisn,
                  decoration: InputDecoration(
                      labelText: "NISN",
                      labelStyle: TextStyle(
                        fontSize: 14,
                        color: Colors.black,
                      ),
                      floatingLabelBehavior: FloatingLabelBehavior.always),
                ),
                SizedBox(
                  height: AdaptiveSize.height(10),
                ),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    Container(
                      height: AdaptiveSize.height(100),
                      width: AdaptiveSize.width(100),
                      color: Colors.grey,
                      child: Builder(builder: (_) {
                        if (_image == null) {
                          return Center(
                            child: Text("NO FILE"),
                          );
                        } else {
                          return ClipRRect(
                            borderRadius: BorderRadius.circular(50),
                            child: Image.file(
                              _image,
                              width: AdaptiveSize.width(340),
                              height: AdaptiveSize.height(250),
                              fit: BoxFit.fitHeight,
                            ),
                          );
                        }
                      }),
                    ),
                    SizedBox(
                      width: AdaptiveSize.width(10),
                    ),
                    RaisedButton(
                      onPressed: () {
                        _imgFromGallery();
                      },
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(
                          AdaptiveSize.width(5),
                        ),
                      ),
                      // padding: EdgeInsets.symmetric(
                      //   vertical: AdaptiveSize.height(10),
                      //   horizontal: AdaptiveSize.width(2),
                      // ),
                      color: Colors.blueAccent,
                      elevation: 3,
                      child: Text(
                        "Pilih",
                        style: TextStyle(
                          fontSize: 10,
                          fontWeight: FontWeight.bold,
                          color: Colors.white,
                        ),
                      ),
                    )
                  ],
                ),
                SizedBox(
                  height: AdaptiveSize.height(30),
                ),
                GestureDetector(
                  onTap: () {
                    if (isEdit == false && isRegis == false) {
                      createData();
                      Navigator.of(context).pop();
                      Navigator.of(context).pop();
                    } else if (isEdit == false && isRegis == true) {
                      createData();
                      Navigator.of(context).pop();
                    } else if (isEdit == true && isRegis == false) {
                      updateData();
                      Navigator.of(context).pop();
                      Navigator.of(context).pop();
                    }
                  },
                  child: Container(
                    width: MediaQuery.of(context).size.width,
                    height: AdaptiveSize.height(50),
                    decoration: BoxDecoration(
                      borderRadius:
                          BorderRadius.circular(AdaptiveSize.width(5)),
                      color: Colors.blueAccent,
                    ),
                    child: Center(
                      child: Text(
                        "Simpan",
                        style: TextStyle(
                          fontSize: 16,
                          color: Colors.white,
                        ),
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  height: AdaptiveSize.height(10),
                ),
                Builder(builder: (_) {
                  if (isEdit == true) {
                    return GestureDetector(
                      onTap: () {
                        deleteData();
                        Navigator.of(context).pop();
                        Navigator.of(context).pop();
                      },
                      child: Container(
                        width: MediaQuery.of(context).size.width,
                        height: AdaptiveSize.height(50),
                        decoration: BoxDecoration(
                          borderRadius:
                              BorderRadius.circular(AdaptiveSize.width(5)),
                          color: Colors.redAccent,
                        ),
                        child: Center(
                          child: Text(
                            "Hapus",
                            style: TextStyle(
                              fontSize: 16,
                              color: Colors.white,
                            ),
                          ),
                        ),
                      ),
                    );
                  } else {
                    return Container();
                  }
                })
              ],
            ),
          )
        ],
      ),
    );
  }
}
