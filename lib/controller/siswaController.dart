import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;

class SiswaController extends GetxController {
  var siswa = [].obs;
  var login = [].obs;
  var jur = [].obs;
  int logg;
  bool enable;

  getListSiswa() async {
    final String endpoint = "https://new-sekolah.herokuapp.com/getSiswa.php";

    final response = await http.get(endpoint);

    final Map json = await jsonDecode(response.body);

    print(response.statusCode);

    //debugPrint(json.toString(), wrapWidth: 1000);

    if (response.statusCode == 200) {
      siswa.value = json["data"];
    }
    print(siswa);
  }

  deleteDataSiswa(String id) async {
    final String endpoint =
        "https://new-sekolah.herokuapp.com/deleteSiswa.php?id=$id";

    final response = await http.delete(endpoint);

    final Map json = await jsonDecode(response.body);

    print(response.statusCode);

    //debugPrint(json.toString(), wrapWidth: 1000);

    if (response.statusCode == 200) {
      return true;
    }
    print("sukses");
  }

  Future<bool> putDataSiswa(String id, String nama, String image, String alamat,
      int notelp, String sekolahA, String ortu, String nisn) async {
    final String endpoint =
        "https://new-sekolah.herokuapp.com/updateSiswa.php?id=$id&nama=$nama&image=$image&alamat=$alamat&notelp=$notelp&sekolah_asal=$sekolahA&nama_ortu=$ortu&nisn=$nisn";

    final response = await http.delete(endpoint);

    final Map json = await jsonDecode(response.body);

    print(response.statusCode);

    //debugPrint(json.toString(), wrapWidth: 1000);

    if (response.statusCode == 200) {
      return true;
    }
    print("sukses");
    return false;
  }

  Future<bool> createDataSiswa(String nama, String image, String alamat,
      int notelp, String sekolahA, String ortu, String nisn) async {
    final String endpoint =
        "https://new-sekolah.herokuapp.com/createSiswa.php?&nama=$nama&image=$image&alamat=$alamat&notelp=$notelp&sekolah_asal=$sekolahA&nama_ortu=$ortu&nisn=$nisn";

    final response = await http.delete(endpoint);

    final Map json = await jsonDecode(response.body);

    print(response.statusCode);

    //debugPrint(json.toString(), wrapWidth: 1000);

    if (response.statusCode == 200) {
      return true;
    }
    print("sukses");
    return false;
  }

  auth(String nisn) async {
    final String endpoint =
        "https://new-sekolah.herokuapp.com/login.php?nisn='$nisn'";

    final response = await http.get(endpoint);

    final Map json = await jsonDecode(response.body);

    print(response.statusCode);

    //debugPrint(json.toString(), wrapWidth: 1000);

    if (response.statusCode == 200) {
      login.value = json["data"];
      logg = 1;
    }
    print(login);
  }

  getDataJurusan() async {
    final String endpoint = "https://new-sekolah.herokuapp.com/postSiswa.php";

    final response = await http.get(endpoint);

    final Map json = await jsonDecode(response.body);

    print(response.statusCode);

    //debugPrint(json.toString(), wrapWidth: 1000);

    if (response.statusCode == 200) {
      jur.value = json["data"];
    }
    print(jur);
  }

  Future<bool> createPenjurusan(String idsiswa, String idjur) async {
    final String endpoint =
        "https://new-sekolah.herokuapp.com/createDetail.php?id_siswa=$idsiswa&id_jur=$idjur";

    final response = await http.delete(endpoint);

    final Map json = await jsonDecode(response.body);

    print(response.statusCode);

    //debugPrint(json.toString(), wrapWidth: 1000);

    if (response.statusCode == 200) {
      return true;
    }
    print("sukses");
    return false;
  }
}
