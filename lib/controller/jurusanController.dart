import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;

class JurusanController extends GetxController {
  var jurusan = [].obs;

  getDataJurusan() async {
    final String endpoint = "https://new-sekolah.herokuapp.com/getJurusan.php";

    final response = await http.get(endpoint);

    final Map json = await jsonDecode(response.body);

    print(response.statusCode);

    //debugPrint(json.toString(), wrapWidth: 1000);

    if (response.statusCode == 200) {
      jurusan.value = json["data"];
    }
    print(jurusan);
  }

  Future<bool> createJurusan(String nama, int kuota) async {
    final String endpoint =
        "https://new-sekolah.herokuapp.com/createJurusan.php?nama=$nama&kuotamax=$kuota";

    final response = await http.delete(endpoint);

    final Map json = await jsonDecode(response.body);

    print(response.statusCode);

    //debugPrint(json.toString(), wrapWidth: 1000);

    if (response.statusCode == 200) {
      return true;
    }
    print("sukses");
    return false;
  }
}
